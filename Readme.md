:00000000004F3424
.text:00000000004F3424 ; void __cdecl strbuf_addch_34(strbuf *sb, int c)
.text:00000000004F3424 strbuf_addch_34 proc near               ; CODE XREF: strbuf_realpath_1+22C↓p
.text:00000000004F3424                                         ; strbuf_realpath_1+401↓p
.text:00000000004F3424
.text:00000000004F3424 c               = dword ptr -0Ch
.text:00000000004F3424 sb              = qword ptr -8
.text:00000000004F3424
.text:00000000004F3424 ; __unwind {
.text:00000000004F3424                 push    rbp
.text:00000000004F3425                 mov     rbp, rsp
.text:00000000004F3428                 sub     rsp, 10h
.text:00000000004F342C                 mov     [rbp+sb], rdi
.text:00000000004F3430                 mov     [rbp+c], esi
.text:00000000004F3433                 mov     rax, [rbp+sb]
.text:00000000004F3437                 mov     rdi, rax        ; sb
.text:00000000004F343A                 call    strbuf_avail_34
.text:00000000004F343F                 test    rax, rax
.text:00000000004F3442                 jnz     short loc_4F3455
.text:00000000004F3444                 mov     rax, [rbp+sb]
.text:00000000004F3448                 mov     esi, 1          ; extra
.text:00000000004F344D                 mov     rdi, rax        ; sb
.text:00000000004F3450                 call    strbuf_grow
.text:00000000004F3455
.text:00000000004F3455 loc_4F3455:                             ; CODE XREF: strbuf_addch_34+1E↑j
.text:00000000004F3455                 mov     rax, [rbp+sb]
.text:00000000004F3459                 mov     rsi, [rax+10h]
.text:00000000004F345D                 mov     rax, [rbp+sb]
.text:00000000004F3461                 mov     rax, [rax+8]
.text:00000000004F3465                 lea     rcx, [rax+1]
.text:00000000004F3469                 mov     rdx, [rbp+sb]
.text:00000000004F346D                 mov     [rdx+8], rcx
.text:00000000004F3471                 add     rax, rsi
.text:00000000004F3474                 mov     edx, [rbp+c]
.text:00000000004F3477                 mov     [rax], dl
.text:00000000004F3479                 mov     rax, [rbp+sb]
.text:00000000004F347D                 mov     rdx, [rax+10h]
.text:00000000004F3481                 mov     rax, [rbp+sb]
.text:00000000004F3485                 mov     rax, [rax+8]
.text:00000000004F3489                 add     rax, rdx
.text:00000000004F348C                 mov     byte ptr [rax], 0
.text:00000000004F348F                 nop
.text:00000000004F3490                 leave
.text:00000000004F3491                 retn
